PRTG Device Template for Nasuni Filer Storage device
==============================================


This project contains all the files necessary to integrate the Nasuni Filer
into PRTG for auto discovery and sensor creation.

Sensors
====
![Nasuni health](./Images/Nasuni-Health.png)
Nasuni health


![Nasuni Stats](./Images/Nasuni-Stats.png)
Nasuni Stats

![Nasuni Volume](./Images/Nasuni-Volume.png)
Nasuni Volume

Download Instructions
=========================
[A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/nasuni-filer/-/jobs/artifacts/master/download?job=PRTGDistZip) 


Installation Instructions
=========================
Please see: [Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)
